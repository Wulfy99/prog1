public class SzuloGyerek {

	class Szulo {
		String pelda1() { return  "pelda1"; }
		String pelda2() { return  "pelda2"; }
		String pelda3() { return  "pelda3"; }
	}
	class Gyerek extends Szulo{
		String pelda3() { return  "valami más"; }
	}
	
	public static void main(String[] args) {
		
		SzuloGyerek szgy = new SzuloGyerek();
		Szulo gyerek = szgy.new Gyerek();
		
		System.out.println("Szulo gyerek = szgy.new Gyerek();");
		System.out.println(gyerek.pelda1());
		System.out.println(gyerek.pelda2());
		System.out.println(gyerek.pelda3());

	}
}
