public class Liskovra_figyel {

	class Allat{

	}

	class RagadozoAllat extends Allat{

		void vadaszik(){
			System.out.println( this.toString() + " ... vadaszik ... ");
		}

	}


	class Zebra extends Allat{

	}
	
	class Oroszlan extends RagadozoAllat{

	}
	
	public static void main(String[] args) {
		Liskovra_figyel liskov = new Liskovra_figyel();
		
		Zebra zebra = liskov.new Zebra();
		Oroszlan oroszlan = liskov.new Oroszlan();
		
	//	zebra.vadaszik();
		oroszlan.vadaszik();
	}
}

