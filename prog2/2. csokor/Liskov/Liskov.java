public class Liskov {

	class Allat{

		
		void vadaszik(){
			System.out.println( this.toString() + " ... vadaszik ... ");
		}

	}

	class Zebra extends Allat{

	}
	
	class Oroszlan extends Allat{

	}
	
	public static void main(String[] args) {
		Liskov liskov = new Liskov();
		
		Zebra zebra = liskov.new Zebra();
		Oroszlan oroszlan = liskov.new Oroszlan();
		
		zebra.vadaszik();
		oroszlan.vadaszik();
	}
}
