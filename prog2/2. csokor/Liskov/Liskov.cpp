#include <iostream>

using namespace std;

class Allat {
		
public:
	void vadaszik(){
		cout << "vadaszik ..." << endl;
	}

};

class Oroszlan: public Allat {

};

class Zebra : public Allat {

};

int main ( int argc, char **argv ){
     
	Oroszlan oroszlan;
	Zebra zebra;

	zebra.vadaszik();
	oroszlan.vadaszik();

	return 0;
}

