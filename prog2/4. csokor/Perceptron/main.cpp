#include "percep.hpp"
#include <iostream>
#include "png++/png.hpp"
#include <fstream>

int main(int argc, char const *argv[]) {

	png::image <png::rgb_pixel> png_image(argv[1]);

	int size = png_image.get_width() * png_image.get_height();

	Perceptron* p = new Perceptron(3,size,256,1);

	double* imager = new double[size];

	for(int i = 0; i < png_image.get_width(); i++){
		for(int j = 0; j < png_image.get_height(); j++){

		imager[ i * png_image.get_width() + j ] = png_image[i][j].red;

		}
	}

	double value = (*p) (imager);
	std::cout << value << std::endl;

	png::image <png::rgb_pixel> png_res ((*p)(png_image));
	png_res.write("return_image.png");
	std::fstream output_file ("weights.txt", std::ios_base::out);

	//p->save(output_file);
	p->avg();

	output_file.close();
	delete p;
	delete imager;

	return 0;
}
