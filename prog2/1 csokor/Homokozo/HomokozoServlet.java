import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Homokozo_web
 */
@WebServlet("/")
public class HomokozoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomokozoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
/*	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  
		String Text_field = request.getParameter("text_field");
		
		LZWBinFa binfa = new LZWBinFa();
		
		for(int i = 0; i < Text_field.length(); i++) {
			char ch = Text_field.charAt(i);
			for(int j = 0; j < 8; j++) {
				if((ch & 0x80) == 0x80) {
					binfa.push_back('1');
				} else {
					binfa.push_back('0');
				}
				ch <<= 1;				
			}
		}
		
		PrintWriter pw = response.getWriter();  
		binfa.kiir(pw);
		pw.close();	
	}

}